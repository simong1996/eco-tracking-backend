import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Datarow } from "./Datarow";
import { User } from "./User";

@Entity()
export class Account extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column("varchar", { length: 225 })
  name: string;

  @Column("decimal")
  startingBalance: number;

  @Column("boolean", { default: false })
  main: boolean;

  @OneToMany(() => Datarow, (datarow) => datarow.toAccount)
  to: Datarow[];

  @OneToMany(() => Datarow, (datarow) => datarow.fromAccount)
  from: Datarow[];

  @ManyToOne(() => User)
  user: User;
}
