import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity,
  ManyToOne,
} from "typeorm";

import { User } from "./User";
import { ColumnTypeArray } from "../types/Enums/ColumnTypes";

export type ColumnTypes =
  | "amount"
  | "transactionDate"
  | "identifier"
  | "description"
  | "category"
  | "transactionType"
  | "toAccount"
  | "fromAccount"
  | "isUnusual";
@Entity()
export class Datamap extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column("varchar", { length: 225 })
  originKey: string;

  @Column({
    name: "newKey",
    type: "enum",
    enum: ColumnTypeArray,
  })
  newKey: ColumnTypes;

  @ManyToOne(() => User)
  user: User;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;
}
