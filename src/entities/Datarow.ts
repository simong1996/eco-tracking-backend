import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import { User } from "./User";
import { Category } from "./Category";
import { TransactionTypeArray } from "../types/Enums/TransactionType";
import { Account } from "./Account";

export type TransactionType = "transfer" | "income" | "expense";

@Entity()
export class Datarow extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column("decimal")
  amount: number;

  @Column({ type: "timestamptz", nullable: true })
  transactionDate: Date;

  @Column("varchar", { length: 225 })
  identifier: string;

  @Column("varchar", { length: 225 })
  description: string | null;

  @Column("boolean", { default: false })
  isUnusual: boolean;

  @Column({
    name: "transactionType",
    type: "enum",
    enum: TransactionTypeArray,
  })
  transactionType: TransactionType;

  @ManyToOne(() => Category)
  category: Category;

  @ManyToOne(() => User)
  user: User;

  @ManyToOne(() => Account, (account) => account.to)
  toAccount: Account | null;

  @ManyToOne(() => Account, (account) => account.from)
  fromAccount: Account | null;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;
}
