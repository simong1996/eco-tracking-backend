declare global {
  namespace NodeJS {
    interface ProcessEnv {
      DATABASE_CONFIG: string;
      REDIS_TLS_URL: string;
      DATABASE_URL: string;
      EMAIL_USERNAME: string;
      EMAIL_PASSWORD: string;
      CLIENT_URL: string;
      SESSION_SECRET: string;
    }
  }
}

export {}
