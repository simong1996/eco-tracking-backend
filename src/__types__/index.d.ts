import { User } from "src/entities/User";
import { Datamap } from "src/entities/Datamap";
import { Category } from "src/entities/Category";
import { Datarow } from "src/entities/Datarow";
import { Account } from "src/entities/Account";
import { Session } from "express-session";
import { Response, Request } from "express";
import Redis from "ioredis";

declare module "express-session" {
  interface Session {
    userId: string;
  }
}

export type ContextType = {
  db: {
    User: typeof User;
    Datamap: typeof Datamap;
    Category: typeof Category;
    Datarow: typeof Datarow;
    Account: typeof Account;
  };
  res: Response;
  req: Request;
  redis: Redis;
};

export type DefaultContext = {
  response: Response;
  request: Request;
};
