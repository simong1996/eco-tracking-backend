import { booleanArg, extendType, intArg, stringArg } from "nexus";
import { NexusGenArgTypes } from "src/generated/typings";
import { ContextType } from "src/__types__";

const Mutation = extendType({
  type: "Mutation",
  definition(t) {
    t.field("createAccount", {
      type: "Account",
      args: {
        userId: stringArg(
          { required: true }
          ),
        startingBalance: intArg(
          { required: true }
          ),
        name: stringArg(
          { required: true }
          ),
        main: booleanArg(),
      },
      resolve: async (
        _,
        {
          userId,
          startingBalance,
          name,
          main,
        }: NexusGenArgTypes["Mutation"]["createAccount"],
        ctx: ContextType
      ) => {
        const user = await ctx.db.User.findOne({
          where: { id: userId },
        });

        const existingAcc = await ctx.db.Account.findOne({
          where: [{ name, user }, ...(main ? [{ main: true, user }] : [])],
        });

        if (existingAcc) {
          if (existingAcc.name === name) {
            throw new Error("Account with this name already exists.");
          } else {
            throw new Error("You already have an account set as main.");
          }
        }

        const account = await ctx.db.Account.create({
          user,
          name,
          startingBalance,
          ...(main ? { main } : {}),
        }).save();

        return account;
      },
    });
  },
});

export default [Mutation];
