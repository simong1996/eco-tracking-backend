import { list, objectType } from "nexus";

import Mutation from "./mutations";
import Query from "./queries";

const Account = objectType({
  name: "Account",
  definition(t) {
    t.string("id");
    t.string("name");
    t.float("startingBalance");
    t.boolean("main");
    t.field("to", { type: list("Datarow") });
    t.field("from", { type: list("Datarow") });
    t.field("user", { type: "User" });
  },
});

export default [Account, Mutation, Query];
