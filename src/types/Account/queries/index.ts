import { extendType, stringArg } from "nexus";
import { NexusGenArgTypes } from "src/generated/typings";
import { ContextType } from "src/__types__";

const Query = extendType({
  type: "Query",
  definition(t) {
    t.list.field("accounts", {
      type: "Account",
      args: {
        userId: stringArg({ required: true }),
      },
      resolve: async (
        _,
        { userId }: NexusGenArgTypes["Query"]["accounts"],
        ctx: ContextType
      ) => {
        const accounts = await ctx.db.Account.find({
          where: { user: { id: userId } },
        });
        
        return accounts;
      },
    });
  },
});

export default [Query];
