import { DateTimeResolver } from "graphql-scalars";
import { asNexusMethod } from "nexus";

export default asNexusMethod(DateTimeResolver, "date");
