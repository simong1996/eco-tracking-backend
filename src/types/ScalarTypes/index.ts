import GraphQLDateTime from "./DateTime";
import GraphQLDate from "./Date";

export default [GraphQLDateTime, GraphQLDate];
