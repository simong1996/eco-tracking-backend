import { DateResolver } from "graphql-scalars";
import { asNexusMethod } from "nexus";

export default asNexusMethod(DateResolver, "date");
