import { arg, extendType, inputObjectType, list, stringArg } from "nexus";
import { Datarow } from "src/entities/Datarow";
import { NexusGenArgTypes } from "src/generated/typings";
import { ContextType } from "src/__types__";

const DatarowsCreateInput = inputObjectType({
  name: "DatarowsCreateInput",
  nonNullDefaults: {
    input: true,
  },
  definition(t) {
    t.float("amount");
    t.field("transactionDate", {
      type: "DateTime",
      required: false,
    });
    t.string("identifier");
    t.string("description", { required: false });
    t.field("transactionType", { type: "TransactionType" });
    t.string("category");
    t.string("toAccount", { required: false });
    t.string("fromAccount", { required: false });
    t.boolean("isUnusual", { default: false });
  },
});

const Mutation = extendType({
  type: "Mutation",
  definition(t) {
    t.field("createDatarows", {
      type: list("Datarow"),
      args: {
        userId: stringArg(),
        data: arg({
          type: "DatarowsCreateInput",
          list: true,
          required: true,
        }),
      },
      resolve: async (
        _,
        { data, userId }: NexusGenArgTypes["Mutation"]["createDatarows"],

        ctx: ContextType
      ) => {
        const user = await ctx.db.User.findOne({
          where: { id: userId },
        });

        const rows: Datarow[] = [];
        for (let i = 0; i < data?.length; i++) {
          const {
            category: categoryId,
            toAccount: toAccountId,
            fromAccount: fromAccountId,
            amount,
            ...rest
          } = data?.[i];

          const category = await ctx.db.Category.findOne({
            where: { id: categoryId },
          });

          let toAccount;
          if (toAccountId) {
            toAccount = await ctx.db.Account.findOne({
              where: { id: toAccountId },
            });
          }

          let fromAccount;
          if (fromAccountId) {
            fromAccount = await ctx.db.Account.findOne({
              where: { id: fromAccountId },
            });
          }

          const absAmount = Math.abs(amount);
          
          const newRow = await ctx.db.Datarow.create({
            user,
            category,
            amount: absAmount,
            ...(toAccount ? { toAccount } : {}),
            ...(fromAccount ? { fromAccount } : {}),
            ...rest,
          }).save();

          rows?.push(newRow);
        }
        return rows;
      },
    });
  },
});

export default [Mutation, DatarowsCreateInput];
