import { objectType } from "nexus";

import Mutation from "./mutations";
import Query from "./queries";

const Datarow = objectType({
  name: "Datarow",
  definition(t) {
    t.string("id");
    t.float("amount");
    t.field("transactionDate", { type: "DateTime" });
    t.string("identifier");
    t.string("description");
    t.boolean("isUnusual");
    t.field("transactionType", { type: "TransactionType" });
    t.field("toAccount", { type: "Account" });
    t.field("fromAccount", { type: "Account" });
    t.field("category", { type: "Category" });
    t.field("user", { type: "User" });
    t.field("updatedDate", { type: "DateTime" });
    t.field("createdDate", { type: "DateTime" });
  },
});

export default [Datarow, Mutation, Query];
