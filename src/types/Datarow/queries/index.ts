import { extendType, stringArg } from "nexus";
import { NexusGenArgTypes } from "src/generated/typings";
import { ContextType } from "src/__types__";

const Query = extendType({
  type: "Query",
  definition(t) {
    t.list.field("datarows", {
      type: "Datarow",
      args: {
        userId: stringArg({ required: true }),
      },
      resolve: async (
        _,
        { userId }: NexusGenArgTypes["Query"]["datarows"],
        ctx: ContextType
      ) => {
        const datarows = await ctx.db.Datarow.find({
          where: { user: { id: userId } },
          relations: ["category", "toAccount", "fromAccount"],
        });

        return datarows;
      },
    });
  },
});

export default [Query];
