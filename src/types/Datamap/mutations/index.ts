import { arg, extendType, inputObjectType, list, stringArg } from "nexus";
import { Datamap } from "src/entities/Datamap";
import { NexusGenArgTypes } from "src/generated/typings";
import { ContextType } from "src/__types__";

const DatamapsCreateInput = inputObjectType({
  name: "DatamapsCreateInput",
  nonNullDefaults: {
    input: true,
  },
  definition(t) {
    t.string("originKey");
    t.field("newKey", { type: "ColumnTypes" });
  },
});

const Mutation = extendType({
  type: "Mutation",
  definition(t) {
    t.field("createDatamaps", {
      type: list("Datamap"),
      args: {
        userId: stringArg(),
        data: arg({
          type: "DatamapsCreateInput",
          list: true,
          required: true,
        }),
      },
      resolve: async (
        _,
        { data, userId }: NexusGenArgTypes["Mutation"]["createDatamaps"],
        ctx: ContextType
      ) => {
        const oldDatamaps = await ctx.db.Datamap.find({
          where: { user: { id: userId } },
        });

        const maps: Datamap[] = [];
        for (let i = 0; i < data?.length; i++) {
          const newDatamap = data[i];

          const oldMap = oldDatamaps?.find(
            (_map: Datamap) => _map?.originKey === newDatamap?.originKey
          );

          if (oldMap) {
            if (newDatamap?.newKey !== oldMap?.newKey) {
              const updatedMap = await ctx.db.Datamap.create({
                ...oldMap,
                newKey: newDatamap?.newKey,
              }).save();

              maps.push(updatedMap);
            }
          } else {
            const user = await ctx.db.User.findOne({
              where: { id: userId },
            });

            const newMap = await ctx.db.Datamap.create({
              user,
              originKey: newDatamap?.originKey,
              newKey: newDatamap?.newKey,
            }).save();

            maps.push(newMap);
          }
        }

        return maps;
      },
    });
  },
});

export default [Mutation, DatamapsCreateInput];
