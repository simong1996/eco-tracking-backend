import { objectType } from "nexus";

import Mutations from "./mutations";
import Query from "./queries";

const Datamap = objectType({
  name: "Datamap",
  definition(t) {
    t.string("id");
    t.string("originKey");
    t.field("user", { type: "User" });
    t.field("newKey", { type: "ColumnTypes" });
    t.field("updatedDate", { type: "DateTime" });
    t.field("createdDate", { type: "DateTime" });
  },
});

export default [Datamap, Mutations, Query];
