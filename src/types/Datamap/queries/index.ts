import { extendType, stringArg } from "nexus";
import { NexusGenArgTypes } from "src/generated/typings";
import { ContextType } from "src/__types__";

const Query = extendType({
  type: "Query",
  definition(t) {
    t.list.field("datamaps", {
      type: "Datamap",
      args: {
        userId: stringArg({ required: true }),
      },
      resolve: async (
        _,
        { userId }: NexusGenArgTypes["Query"]["datamaps"],
        ctx: ContextType
      ) => {
        const datamaps = await ctx.db.Datamap.find({
          where: { user: { id: userId } },
        });

        return datamaps;
      },
    });
  },
});

export default [Query];
