import { extendType, list, nonNull, objectType, stringArg } from "nexus";
import { ContextType } from "src/__types__";
import argon2 from "argon2";
import * as EmailValidator from "email-validator";
import { NexusGenArgTypes } from "src/generated/typings";
import { v4 } from "uuid";
import { sendEmail } from "../../../utils/sendEmail";
import { User } from "../../../entities/User";
import {
  COOKIE_NAME,
  FORGOT_PASSWORD_PREFIX,
  VERIFY_EMAIL_PREFIX,
} from "../../../utils/constants";

const FieldError = objectType({
  name: "FieldError",
  definition(t) {
    t.string("field", { required: true });
    t.string("message", { required: true });
  },
});

const UserResponse = objectType({
  name: "UserResponse",
  definition(t) {
    t.field("fieldError", { type: list(nonNull(FieldError)) });
    t.field("user", { type: "User" });
  },
});

const Mutation = extendType({
  type: "Mutation",
  definition(t) {
    t.field("createUser", {
      type: UserResponse,
      args: {
        email: stringArg({ required: true }),
        password: stringArg({ required: true }),
      },
      resolve: async (
        _,
        { email, password }: NexusGenArgTypes["Mutation"]["createUser"],
        ctx: ContextType
      ) => {
        if (!EmailValidator.validate(email)) {
          return {
            fieldError: [
              {
                field: "email",
                message: "Thats not an email",
              },
            ],
          };
        }

        if (password.length <= 3) {
          return {
            fieldError: [
              {
                field: "password",
                message: "Password has to be at least 3 characters",
              },
            ],
          };
        }

        const hashedPassword = await argon2.hash(password);

        const user = ctx.db.User.create({
          email: email.toLowerCase(),
          password: hashedPassword,
        });
        try {
          await user.save();
        } catch (err) {
          if (err.code === "23505") {
            return {
              fieldError: [
                {
                  field: "email",
                  message: "Email is already registered",
                },
              ],
            };
          }
        }

        const token = v4();

        await ctx.redis.set(
          VERIFY_EMAIL_PREFIX + token,
          user.id,
          "EX",
          1000 * 60 * 60 * 24 * 365 * 10 // 10 Years
        );

        await sendEmail(
          email,
          `<a href="${process.env.CLIENT_URL}/verify/${token}">Verify Email address</a>`,
          "Verify your Eco account"
        );

        ctx.req.session.userId = user.id;

        return { user };
      },
    });
    t.field("login", {
      type: UserResponse,
      args: {
        email: stringArg({ required: true }),
        password: stringArg({ required: true }),
      },
      resolve: async (
        _,
        { email, password }: NexusGenArgTypes["Mutation"]["login"],
        ctx: ContextType
      ) => {
        const user = await ctx.db.User.findOne({ email: email.toLowerCase() });

        if (!user) {
          return {
            fieldError: [
              {
                field: "email",
                message: "That email does not exist",
              },
            ],
          };
        }

        const valid = await argon2.verify(user?.password, password);

        if (!valid) {
          return {
            fieldError: [
              {
                field: "password",
                message: "Incorrect password",
              },
            ],
          };
        }

        console.log(user.id)
        ctx.req.session.userId = user.id;

        console.log(ctx.req.session.userId)

        return { user };
      },
    });
    t.field("logout", {
      type: "Boolean",
      resolve: async (_, __, ctx: ContextType) => {
        return new Promise((resolve) =>
          ctx.req.session.destroy((err) => {
            ctx.res.clearCookie(COOKIE_NAME);
            if (err) {
              console.log(err);
              resolve(false);
              return;
            }

            resolve(true);
          })
        );
      },
    });
    t.field("forgotPassword", {
      type: "Boolean",
      args: {
        email: stringArg({ required: true }),
      },
      resolve: async (
        _,
        { email }: NexusGenArgTypes["Mutation"]["forgotPassword"],
        ctx: ContextType
      ) => {
        const user = await ctx.db.User.findOne({ email: email.toLowerCase() });

        if (!user) {
          // Email was not found
          return true;
        }

        const token = v4();

        await ctx.redis.set(
          FORGOT_PASSWORD_PREFIX + token,
          user.id,
          "EX",
          1000 * 60 * 24 * 3
        ); // 3 days

        await sendEmail(
          email,
          `<a href="${process.env.CLIENT_URL}/change-password/${token}">reset password</a>`,
          "Someone requested a password change, if this was not you. Ignore this email."
        );

        return true;
      },
    });
    t.field("changePassword", {
      type: UserResponse,
      args: {
        token: stringArg({ required: true }),
        newPassword: stringArg({ required: true }),
      },
      resolve: async (
        _,
        { newPassword, token }: NexusGenArgTypes["Mutation"]["changePassword"],
        ctx: ContextType
      ) => {
        if (newPassword.length <= 3) {
          return {
            fieldError: [
              {
                field: "newPassword",
                message: "Password has to be at least 3 characters",
              },
            ],
          };
        }

        const key = FORGOT_PASSWORD_PREFIX + token;
        const userId = await ctx.redis.get(key);
        if (!userId) {
          return {
            fieldError: [
              {
                field: "token",
                message: "token expired",
              },
            ],
          };
        }

        const user = await ctx.db.User.findOne({ id: userId });

        if (!user) {
          return {
            fieldError: [
              {
                field: "token",
                message: "user no longer exists",
              },
            ],
          };
        }

        const hashedPassword = await argon2.hash(newPassword);
        await User.update({ id: userId }, { password: hashedPassword });

        await ctx.redis.del(key);

        return { user };
      },
    });
    t.field("verify", {
      type: UserResponse,
      args: {
        token: stringArg({ required: true }),
      },
      resolve: async (
        _,
        { token }: NexusGenArgTypes["Mutation"]["verify"],
        ctx: ContextType
      ) => {
        const key = VERIFY_EMAIL_PREFIX + token;
        const userId = await ctx.redis.get(key);
        if (!userId) {
          return {
            fieldError: [
              {
                field: "token",
                message: "token expired",
              },
            ],
          };
        }

        if (ctx.req.session.userId !== userId) {
          return {
            fieldError: [
              {
                field: "token",
                message: "Logged in user doesn't match verified link.",
              },
            ],
          };
        }

        const user = await ctx.db.User.findOne({ id: userId });

        await ctx.redis.del(key);

        // This is probably a tiny bit overkill. Since if the user doesn't exist you're simply redirected to /login
        if (!user) {
          return {
            fieldError: [
              {
                field: "token",
                message: "user no longer exists",
              },
            ],
          };
        }

        await ctx.db.User.update({ id: userId }, { verified: true });

        const newUser = await ctx.db.User.findOne({ id: userId });

        return { user: newUser };
      },
    });
  },
});

export default [Mutation];
