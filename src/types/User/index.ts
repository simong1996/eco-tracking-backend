import { objectType } from "nexus";

import Query from "./queries";
import Mutation from "./mutations";

const User = objectType({
  name: "User",
  definition(t) {
    t.string("id", { required: true });
    t.string("email");
    t.field("updatedDate", { type: "DateTime" });
    t.field("createdDate", { type: "DateTime" });
    t.boolean("verified");
  },
});

export default [User, Query, Mutation];
