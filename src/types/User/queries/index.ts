import { extendType } from "nexus";

const Query = extendType({
  type: "Query",
  definition(t) {
    t.list.field("users", {
      type: "User",
      resolve: async (_, __, ctx) => {
        const users = await ctx.db.User.find();

        return users;
      },
    });
    t.field("isLoggedIn", {
      type: "User",
      resolve: async (_, __, ctx) => {
        console.log(ctx.req.session.userId)
        if (!ctx.req.session.userId) {
          return null;
        }
        const user = await ctx.db.User.findOne({ id: ctx.req.session.userId });

        if (!user) {
          return null;
        }

        return user;
      },
    });
  },
});

export default [Query];
