import { list, objectType } from "nexus";

import Query from "./queries";

const Category = objectType({
  name: "Category",
  definition(t) {
    t.string("id");
    t.string("name");
    t.field("parent", { type: "Category" });
    t.field("children", { type: list("Category") });
    t.field("user", { type: "User" });
    t.field("updatedDate", { type: "DateTime" });
    t.field("createdDate", { type: "DateTime" });
  },
});

export default [Category, Query];
