import { extendType, stringArg } from "nexus";
import { NexusGenArgTypes } from "src/generated/typings";

import { ContextType } from "src/__types__";

const Query = extendType({
  type: "Query",
  definition(t) {
    t.list.field("categories", {
      type: "Category",
      args: {
        userId: stringArg({ required: true }),
      },
      resolve: async (
        _,
        { userId }: NexusGenArgTypes["Query"]["categories"],
        ctx: ContextType
      ) => {
        const categories = await ctx.db.Category.find({
          where: [{ user: { id: userId } }, { user: null }],
          relations: ["parent"],
        });

        return categories;
      },
    });
    t.list.field("categoryParents", {
      type: "Category",
      args: {
        userId: stringArg({ required: true }),
      },
      resolve: async (
        _,
        { userId }: NexusGenArgTypes["Query"]["categoryParents"],
        ctx: ContextType
      ) => {
        const categories = await ctx.db.Category.find({
          where: [
            { user: { id: userId }, parent: null },
            { user: null, parent: null },
          ],
          relations: ["children"],
        });

        return categories;
      },
    });
  },
});

export default [Query];
