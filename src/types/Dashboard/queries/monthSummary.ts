import { getMonth, getYear } from "date-fns";
import { arg, booleanArg, extendType, objectType, stringArg } from "nexus";
import { NexusGenArgTypes } from "src/generated/typings";
import { ContextType } from "src/__types__";
import {
  getTransactionByChildCategory,
  getAssetsStatusByMonth,
} from "./monthMethods";

const ChildCategoryMonthTransfer = objectType({
  name: "ChildCategoryMonthTransfer",
  definition(t) {
    t.string("categoryName");
    t.string("id");
    t.string("key");
    t.float("month", { list: true });
  },
});

const ParentCategoryMonthTransfer = objectType({
  name: "ParentCategoryMonthTransfer",
  definition(t) {
    t.string("categoryName");
    t.string("id");
    t.string("key");
    t.field("children", { type: ChildCategoryMonthTransfer, list: true });
  },
});

const ChildCategoryMonthExpense = objectType({
  name: "ChildCategoryMonthExpense",
  definition(t) {
    t.string("categoryName");
    t.string("id");
    t.string("key");
    t.float("month", { list: true });
  },
});

const ParentCategoryMonthExpense = objectType({
  name: "ParentCategoryMonthExpense",
  definition(t) {
    t.string("categoryName");
    t.string("id");
    t.string("key");
    t.field("children", { type: ChildCategoryMonthExpense, list: true });
  },
});

const ChildCategoryMonthIncome = objectType({
  name: "ChildCategoryMonthIncome",
  definition(t) {
    t.string("categoryName");
    t.string("id");
    t.string("key");
    t.float("month", { list: true });
  },
});

const ParentCategoryMonthIncome = objectType({
  name: "ParentCategoryMonthIncome",
  definition(t) {
    t.string("categoryName");
    t.string("id");
    t.string("key");
    t.field("children", { type: ChildCategoryMonthIncome, list: true });
  },
});

const AssetMonthResponse = objectType({
  name: "AssetMonthResponse",
  definition(t) {
    t.string("id");
    t.string("name");
    t.float("startingBalance");
    t.boolean("main");
    t.float("month");
  },
});

const MonthResponse = objectType({
  name: "MonthResponse",
  definition(t) {
    t.field("assets", { type: AssetMonthResponse, list: true });
    t.field("expenses", { type: ParentCategoryMonthExpense, list: true });
    t.field("transfers", { type: ParentCategoryMonthTransfer, list: true });
    t.field("income", { type: ParentCategoryMonthIncome, list: true });
    t.field("datarows", { type: "Datarow", list: true });
  },
});

const Query = extendType({
  type: "Query",
  definition(t) {
    t.field("monthSummary", {
      type: MonthResponse,
      args: {
        year: arg({
          type: "Date",
          required: true,
        }),
        userId: stringArg({ required: true }),
        includeUnusual: booleanArg({ required: true, default: false }),
      },
      resolve: async (
        _,
        { year, userId, includeUnusual }: NexusGenArgTypes["Query"]["summary"],
        ctx: ContextType
      ) => {
        // Get all of the dataRows
        const dataRows = await ctx.db.Datarow.find({
          where: { user: { id: userId } },
          relations: ["category", "toAccount", "fromAccount"],
        });

        // Get all parent categories
        const parentCategories = await ctx.db.Category.find({
          where: [
            { user: { id: userId }, parent: null },
            { user: null, parent: null },
          ],
        });

        // Convert date from request to year
        const requestedYear = getYear(new Date(year));
        // Convert date from request to month
        const requestedMonth = getMonth(new Date(year));

        // Datarows
        const filteredDataRows = dataRows?.filter((row) => {
          // Get the rows year of transaction
          const rowYear = getYear(new Date(row.transactionDate));
          // Get the rows month of transaction
          const rowMonthNr = getMonth(new Date(row.transactionDate));
          return requestedYear === rowYear && requestedMonth === rowMonthNr;
        });

        // const expenses =
        const expenses = getTransactionByChildCategory({
          year,
          dataRows,
          includeUnusual,
          parentCategories,
          transactionType: "expense",
        });

        // const income =
        const income = getTransactionByChildCategory({
          year,
          dataRows,
          includeUnusual,
          parentCategories,
          transactionType: "income",
        });

        // const transfers =
        const transfers = getTransactionByChildCategory({
          year,
          dataRows,
          includeUnusual,
          parentCategories,
          transactionType: "transfer",
        });

        const assets = getAssetsStatusByMonth({
          year,
          dataRows,
        });

        return { assets, expenses, transfers, income, datarows: filteredDataRows };
      },
    });
  },
});

export default [
  Query,
  MonthResponse,
  ParentCategoryMonthIncome,
  ChildCategoryMonthIncome,
  ParentCategoryMonthExpense,
  ChildCategoryMonthExpense,
  ParentCategoryMonthTransfer,
  ChildCategoryMonthTransfer,
  AssetMonthResponse,
];
