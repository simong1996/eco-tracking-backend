import Summary from "./summary";
import MonthSummary from "./monthSummary";

export default [Summary, MonthSummary];
