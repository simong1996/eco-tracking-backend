import { arg, booleanArg, extendType, objectType, stringArg } from "nexus";
import { NexusGenArgTypes } from "src/generated/typings";
import { ContextType } from "src/__types__";
import {
  getTransactionByChildCategory,
  getAssetsStatusByMonth,
} from "./methods";

const CategoryMonthsResponse = objectType({
  name: "CategoryMonthsResponse",
  definition(t) {
    t.float("Jan", { list: true });
    t.float("Feb", { list: true });
    t.float("Mar", { list: true });
    t.float("Apr", { list: true });
    t.float("May", { list: true });
    t.float("Jun", { list: true });
    t.float("Jul", { list: true });
    t.float("Aug", { list: true });
    t.float("Sep", { list: true });
    t.float("Oct", { list: true });
    t.float("Nov", { list: true });
    t.float("Dec", { list: true });
  },
});

const ChildCategoryTransfer = objectType({
  name: "ChildCategoryTransfer",
  definition(t) {
    t.string("categoryName");
    t.string("id");
    t.string("key");
    t.field("months", { type: CategoryMonthsResponse });
  },
});

const ParentCategoryTransfer = objectType({
  name: "ParentCategoryTransfer",
  definition(t) {
    t.string("categoryName");
    t.string("id");
    t.string("key");
    t.field("children", { type: ChildCategoryTransfer, list: true });
  },
});

const ChildCategoryExpense = objectType({
  name: "ChildCategoryExpense",
  definition(t) {
    t.string("categoryName");
    t.string("id");
    t.string("key");
    t.field("months", { type: CategoryMonthsResponse });
  },
});

const ParentCategoryExpense = objectType({
  name: "ParentCategoryExpense",
  definition(t) {
    t.string("categoryName");
    t.string("id");
    t.string("key");
    t.field("children", { type: ChildCategoryExpense, list: true });
  },
});
const ChildCategoryIncome = objectType({
  name: "ChildCategoryIncome",
  definition(t) {
    t.string("categoryName");
    t.string("id");
    t.string("key");
    t.field("months", { type: CategoryMonthsResponse });
  },
});

const ParentCategoryIncome = objectType({
  name: "ParentCategoryIncome",
  definition(t) {
    t.string("categoryName");
    t.string("id");
    t.string("key");
    t.field("children", { type: ChildCategoryIncome, list: true });
  },
});

const AssetMonthsResponse = objectType({
  name: "AssetMonthsResponse",
  definition(t) {
    t.float("Jan");
    t.float("Feb");
    t.float("Mar");
    t.float("Apr");
    t.float("May");
    t.float("Jun");
    t.float("Jul");
    t.float("Aug");
    t.float("Sep");
    t.float("Oct");
    t.float("Nov");
    t.float("Dec");
  },
});

const AssetResponse = objectType({
  name: "AssetResponse",
  definition(t) {
    t.string("id");
    t.string("name");
    t.float("startingBalance");
    t.boolean("main");
    t.field("months", { type: AssetMonthsResponse });
  },
});

const SummaryResponse = objectType({
  name: "SummaryResponse",
  definition(t) {
    t.field("assets", { type: AssetResponse, list: true });
    t.field("expenses", { type: ParentCategoryExpense, list: true });
    t.field("transfers", { type: ParentCategoryTransfer, list: true });
    t.field("income", { type: ParentCategoryIncome, list: true });
  },
});

const Query = extendType({
  type: "Query",
  definition(t) {
    t.field("summary", {
      type: SummaryResponse,
      args: {
        year: arg({
          type: "Date",
          required: true,
        }),
        userId: stringArg({ required: true }),
        includeUnusual: booleanArg({ required: true, default: false }),
      },
      resolve: async (
        _,
        { year, userId, includeUnusual }: NexusGenArgTypes["Query"]["summary"],
        ctx: ContextType
      ) => {
        // Get all of the dataRows
        const dataRows = await ctx.db.Datarow.find({
          where: { user: { id: userId } },
          relations: ["category", "toAccount", "fromAccount"],
        });

        // Get all parent categories
        const parentCategories = await ctx.db.Category.find({
          where: [
            { user: { id: userId }, parent: null },
            { user: null, parent: null },
          ],
        });

        // const expenses =
        const expenses = getTransactionByChildCategory({
          year,
          dataRows,
          includeUnusual,
          parentCategories,
          transactionType: "expense",
        });

        // const income =
        const income = getTransactionByChildCategory({
          year,
          dataRows,
          includeUnusual,
          parentCategories,
          transactionType: "income",
        });

        // const transfers =
        const transfers = getTransactionByChildCategory({
          year,
          dataRows,
          includeUnusual,
          parentCategories,
          transactionType: "transfer",
        });

        const assets = getAssetsStatusByMonth({
          year,
          dataRows,
        });

        return { assets, expenses, transfers, income };
      },
    });
  },
});

export default [
  Query,
  SummaryResponse,
  AssetResponse,
  AssetMonthsResponse,
  ParentCategoryExpense,
  ChildCategoryExpense,
  ParentCategoryTransfer,
  ChildCategoryTransfer,
  ParentCategoryIncome,
  ChildCategoryIncome,
  CategoryMonthsResponse,
];
