import { format, getYear } from "date-fns";
import { Category } from "src/entities/Category";
import { Datarow, TransactionType } from "src/entities/Datarow";

type MonthType =
  | "Jan"
  | "Feb"
  | "Mar"
  | "Apr"
  | "May"
  | "Jun"
  | "Jul"
  | "Aug"
  | "Sep"
  | "Oct"
  | "Nov"
  | "Dec";

type ChildCategoryType = {
  months: { [Property in MonthType]?: number[] };
  categoryName: string;
  id: string;
  key: string;
};

type ParentCategoryType = {
  key: string;
  categoryName: string;
  id: string;
  children: ChildCategoryType[];
};

type AccountsMonthType = {
  id: string;
  name: string;
  startingBalance: number;
  main: boolean;
  months?: {
    [Property in MonthType]?: number;
  };
};

export const getTransactionByChildCategory = ({
  year,
  includeUnusual,
  transactionType,
  dataRows,
  parentCategories,
}: {
  year: any;
  includeUnusual: boolean;
  transactionType: TransactionType;
  dataRows: Datarow[];
  parentCategories: Category[];
}) => {
  // Convert date from request to year
  const requestedYear = getYear(new Date(year));

  const formattedByCategory = dataRows?.reduce((acc, row, i) => {
    // Get the rows year of transaction
    const rowYear = getYear(new Date(row.transactionDate));

    // Filter the dataRows
    if (
      row.transactionType !== transactionType || // match transactionType to method call
      rowYear !== requestedYear || // match year from request to transactionYear
      (!includeUnusual && row.isUnusual) // Check if request wants unusual
    ) {
      return acc;
    }

    // Get row month
    const rowMonth = format(new Date(row?.transactionDate), "MMM");

    // Get parent Category
    const parentCategory = parentCategories?.find((category) =>
      category.childrenIds.includes(row?.category?.id)
    );

    // Typescript check. Parent should never be undefined
    if (!parentCategory) return acc;

    // Get index of parent category
    const indexOfParent = acc?.findIndex(
      (parent) => parent?.id === parentCategory?.id
    );

    // Get index of child category
    const indexOfChild = acc?.[indexOfParent]?.children?.findIndex(
      (child) => child?.id === row?.category?.id
    );

    // Check if category parent already exists
    if (indexOfParent !== -1) {
      // Check if category child already exists
      if (indexOfChild !== -1) {
        // Get current child category object
        let currentChild = acc?.[indexOfParent]?.children?.[indexOfChild];

        // Check if month already exists on object
        if (currentChild.months.hasOwnProperty(rowMonth)) {
          currentChild?.months?.[rowMonth as MonthType]?.push(
            Number(row?.amount)
          );

          acc[indexOfParent].children[indexOfChild] = {
            ...currentChild,
            months: {
              ...currentChild.months,
              [rowMonth]: currentChild?.months?.[rowMonth as MonthType],
            },
          };
        } else {
          acc[indexOfParent].children[indexOfChild] = {
            ...currentChild,
            months: {
              ...currentChild.months,
              [rowMonth]: [Number(row?.amount)],
            },
          };
        }
      } else {
        acc?.[indexOfParent]?.children?.push({
          key: `child.${i}.${transactionType}.${row?.id}`,
          categoryName: row?.category?.name,
          id: row?.category?.id,
          months: {
            [rowMonth]: [Number(row?.amount)],
          },
        });
      }
    } else {
      acc?.push({
        key: `parent.${i}.${transactionType}.${row?.id}`,
        categoryName: parentCategory?.name,
        id: parentCategory?.id,
        children: [
          {
            key: `child.${i}.${transactionType}.${row?.id}`,
            categoryName: row?.category?.name,
            id: row?.category?.id,
            months: {
              [rowMonth]: [Number(row?.amount)],
            },
          },
        ],
      });
    }

    return acc;
  }, [] as ParentCategoryType[]);

  return formattedByCategory;
};

const getNewBalance = ({
  curr,
  changeVal,
  accountKey,
  transactionType,
}: {
  curr: number | undefined;
  changeVal: number;
  accountKey: "fromAccount" | "toAccount";
  transactionType: TransactionType;
}) => {
  // Typescript check
  if (curr === undefined) return 0;

  let newVal = curr;

  if (
    // Subtract
    transactionType === "expense" ||
    (transactionType === "transfer" && accountKey === "fromAccount")
  ) {
    newVal -= Number(changeVal);
  } else if (
    // Add
    transactionType === "income" ||
    (transactionType === "transfer" && accountKey === "toAccount")
  ) {
    newVal += Number(changeVal);
  }

  return newVal;
};

const upsertMonthBalance = ({
  row,
  acc,
  accountKey,
  requestedYear,
}: {
  row: Datarow;
  acc: AccountsMonthType[];
  accountKey: "fromAccount" | "toAccount";
  requestedYear: number;
}) => {
  // Get row month
  const rowMonth = format(new Date(row?.transactionDate), "MMM");

  // Get row year
  const rowYear = getYear(new Date(row?.transactionDate));

  // If the row is ahead of the requested year, ignore it.
  if (requestedYear < rowYear) return acc;

  if (row?.[accountKey]) {
    // Current account Index
    const indexOfAccount = acc?.findIndex(
      (account) => account?.id === row?.[accountKey]?.id
    );

    // Check if account exists in accumulator
    // if the row is the requested year save it in the month object
    // if the row is before the requested year save it in startingBalance
    if (indexOfAccount !== -1) {
      const newVal = getNewBalance({
        curr:
          requestedYear === rowYear
            ? (acc?.[indexOfAccount]?.months?.[rowMonth as MonthType] ?? 0)
            : acc[indexOfAccount].startingBalance,
        accountKey,
        changeVal: row?.amount,
        transactionType: row?.transactionType,
      });

      acc[indexOfAccount] = {
        ...acc[indexOfAccount],
        startingBalance:
          requestedYear === rowYear
            ? acc[indexOfAccount].startingBalance
            : newVal,
        ...(requestedYear === rowYear
          ? {
              months: {
                ...acc[indexOfAccount].months,
                [rowMonth]: newVal,
              },
            }
          : {}),
      };
    } else {
      const newVal = getNewBalance({
        curr:
          requestedYear === rowYear ? 0 : Number(row?.[accountKey]?.startingBalance),
        accountKey,
        changeVal: row?.amount,
        transactionType: row?.transactionType,
      });

      // Create new object
      acc?.push({
        id: row?.[accountKey]?.id || "",
        name: row?.[accountKey]?.name || "",
        main: row?.[accountKey]?.main || false,
        startingBalance:
          requestedYear === rowYear
            ? Number(row?.[accountKey]?.startingBalance) || 0
            : newVal,
        ...(requestedYear === rowYear
          ? {
              months: {
                [rowMonth]: newVal,
              },
            }
          : {}),
      });
    }
  }

  return acc;
};

export const getAssetsStatusByMonth = ({
  year,
  dataRows,
}: {
  year: any;
  dataRows: Datarow[];
}) => {
  const requestedYear = getYear(new Date(year));

  const assetsStatusByMonth = dataRows.reduce((acc, row) => {
    acc = upsertMonthBalance({
      row,
      acc,
      accountKey: "fromAccount",
      requestedYear,
    });

    acc = upsertMonthBalance({
      row,
      acc,
      accountKey: "toAccount",
      requestedYear,
    });

    return acc;
  }, [] as AccountsMonthType[]);

  return assetsStatusByMonth;
};
