import User from "./User";
import Datamap from "./Datamap";
import Category from "./Category";
import Datarow from "./Datarow";
import Account from "./Account";

import ScalarTypes from "./ScalarTypes";
import Enums from "./Enums";
import Dashboard from "./Dashboard";

export default [
  ScalarTypes,
  User,
  Enums,
  Datamap,
  Datarow,
  Category,
  Account,
  Dashboard,
];
