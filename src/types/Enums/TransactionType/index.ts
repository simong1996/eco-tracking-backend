import { enumType } from "nexus";

export const TransactionTypeArray = ["transfer", "income", "expense"];

const _TransactionType = enumType({
  name: "TransactionType",
  members: TransactionTypeArray,
});

export default [_TransactionType];
