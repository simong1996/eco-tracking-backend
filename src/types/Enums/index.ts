import ColumnTypes from "./ColumnTypes";
import TransactionType from "./TransactionType";

export default [ColumnTypes, TransactionType];
