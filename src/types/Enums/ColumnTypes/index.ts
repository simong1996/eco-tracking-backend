import { enumType } from "nexus";

export const ColumnTypeArray = [
  "amount",
  "transactionDate",
  "identifier",
  "description",
  "category",
  "transactionType",
  "toAccount",
  "fromAccount",
  "isUnusual",
];

const _ColumnTypes = enumType({
  name: "ColumnTypes",
  members: ColumnTypeArray,
});

export default [_ColumnTypes];
