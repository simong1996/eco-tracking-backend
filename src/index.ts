import "reflect-metadata";
import dotenv from "dotenv-safe";

import { GraphQLServer } from "graphql-yoga";
import { createConnection, getConnectionOptions } from "typeorm";

import session from "express-session";
import connectRedis from "connect-redis";
import Redis from "ioredis";

import Entities from "./entities";
import { ContextType, DefaultContext } from "./__types__";
import schema from "./schema";
import { permissions } from "./utils/permissions";
import { COOKIE_NAME, __prod__ } from "./utils/constants";

dotenv.config({
  allowEmptyValues: true,
});

const handleGetConnectionOptions = async () => {
  if (process.env.DATABASE_CONFIG === "production") {
    return {
      ...(await getConnectionOptions(process.env.DATABASE_CONFIG)),
      name: "default",
      url: process.env.DATABASE_URL,
    };
  }

  return {
    ...(await getConnectionOptions("local")),
    name: "default",
  };
};

(async () => {
  const RedisStore = connectRedis(session);
  const redisClient =
    process.env.NODE_ENV !== "production"
      ? new Redis()
      : new Redis(process.env.REDIS_TLS_URL || "", {
          tls: {
            rejectUnauthorized: false,
          },
        });

  const connectionOptions = await handleGetConnectionOptions();

  await createConnection(connectionOptions);

  const context = ({ request, response }: DefaultContext) => {
    return {
      db: Entities,
      req: request,
      res: response,
      redis: redisClient,
    } as ContextType;
  };

  const server = new GraphQLServer({
    schema,
    context,
    middlewares: [permissions],
  });

  const opts = {
    port: process.env.PORT || 4000,
    cors: {
      credentials: true,
      origin: [process.env.CLIENT_URL || ""],
    },
  };

  server.express.enable("trust proxy");

  server.express.use(
    session({
      name: COOKIE_NAME,
      store: new RedisStore({ client: redisClient, disableTouch: true }),
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 365 * 10, // 10 Years
        httpOnly: true,
        secure: __prod__,
        sameSite: "none",
      },
      saveUninitialized: false,
      secret: process.env.SESSION_SECRET || "",
      resave: false,
    })
  );

  server.start(opts, () =>
    console.log(`Server is running on port ${process.env.PORT || 4000}`)
  );
})();
