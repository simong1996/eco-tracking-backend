import { rule, shield, and } from "graphql-shield";
import { ContextType } from "src/__types__";

export const isAuthenticated = rule()(async (_, __, ctx: ContextType) => {
  console.log("Hello")
  return !!ctx.req.session.userId;
});

export const isVerified = rule()(async (_, __, ctx: ContextType) => {
  console.log("Hello 3")
  const user = await ctx.db.User.findOne({ id: ctx.req.session.userId });

  console.log("Hello 2")

  if (!user) return false;

  return user.verified;
});

export const permissions = shield({
  Query: {
    datarows: and(isAuthenticated, isVerified),
    accounts: and(isAuthenticated, isVerified),
    categories: and(isAuthenticated, isVerified),
    summary: and(isAuthenticated, isVerified),
    monthSummary: and(isAuthenticated, isVerified),
    datamaps: and(isAuthenticated, isVerified),
    users: and(isAuthenticated, isVerified), // Probably remove this query or create admin permissions
  },
  Mutation: {
    createAccount: and(isAuthenticated, isVerified),
    createDatamaps: and(isAuthenticated, isVerified),
    createDatarows: and(isAuthenticated, isVerified),
    verify: isAuthenticated,
  },
});
