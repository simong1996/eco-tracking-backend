import nodemailer from "nodemailer";

export const sendEmail = async (to: string, html: string, subject: string) => {
  let transporter = nodemailer.createTransport({
    host: "mailcluster.loopia.se",
    secure: true,
    port: 465,
    auth: {
      user: process.env.EMAIL_USERNAME,
      pass: process.env.EMAIL_PASSWORD,
    },
  });

  let info = await transporter.sendMail({
    from: process.env.EMAIL_USERNAME,
    to: to,
    subject,
    html,
  });

  console.log("Message sent: %s", info.messageId);
};
