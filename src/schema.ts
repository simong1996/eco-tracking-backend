import { declarativeWrappingPlugin, makeSchema } from "nexus";

import Types from "./types";

const schema = makeSchema({
  shouldExitAfterGenerateArtifacts:
    process.env.NEXUS_SHOULD_EXIT_AFTER_GENERATE_ARTIFACTS === "true",
  types: Types,
  outputs: {
    schema: __dirname + "/../src/generated/schema.graphql",
    typegen: __dirname + "/../src/generated/typings.ts",
  },
  plugins: [declarativeWrappingPlugin()],
});

export default schema;
