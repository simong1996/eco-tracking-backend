import { Connection, createConnection } from "typeorm";

import { Category } from "../src/entities/Category";
import { defaultCategories } from "./defaultCategories";

let typeormCon: Connection;

(async () => {
  console.log("Opening db connection...");

  typeormCon = await createConnection({
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "",
    password: "",
    database: "ecomatic",
    entities: ["src/entities/*.ts"],
  });

  const categories = await Category.find({
    where: { user: null, parent: null },
    relations: ["children"],
  });

  console.log("Seeding Categories...");

  for (let i = 0; i < defaultCategories.length; i++) {
    const parentCategory = defaultCategories[i];

    const oldParentCategory = categories?.find(
      (category) => category?.name === parentCategory?.name
    );

    let _category;
    if (!oldParentCategory) {
      _category = await Category.create({
        name: parentCategory?.name,
      }).save();

      console.log(`Created parent: ${_category?.name}.`);
    }

    for (let j = 0; j < parentCategory?.children?.length; j++) {
      const childCategory = parentCategory?.children?.[j];

      const oldChildCategory = oldParentCategory?.children?.some(
        (category) => category?.name === childCategory?.name
      );

      if (!oldChildCategory) {
        await Category.create({
          name: childCategory?.name,
          parent: {
            id: _category?.id || oldParentCategory?.id,
          },
        }).save();

        console.log(`Created child: ${childCategory?.name}.`);
      }
    }
  }

  console.log("Seeding finished.");
})().then(() => {
  console.log("Closing db connection...");
  typeormCon.close();
});
