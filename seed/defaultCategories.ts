export const defaultCategories = [
  {
    name: "Children",
    children: [
      {
        name: "Activities",
      },
      {
        name: "Allowance",
      },
      {
        name: "Medicine",
      },
      {
        name: "Childcare",
      },
      {
        name: "Clothes",
      },
      {
        name: "School",
      },
      {
        name: "Toys",
      },
    ],
  },
  {
    name: "Debt",
    children: [
      {
        name: "Credit Card",
      },
      {
        name: "Student Loans",
      },
      {
        name: "Municipal Taxes",
      },
      {
        name: "National Taxes",
      },
      {
        name: "Other",
      },
    ],
  },
  {
    name: "Education",
    children: [
      {
        name: "Lecture",
      },
      {
        name: "Books",
      },
      {
        name: "Music Lessons",
      },
    ],
  },
  {
    name: "Entertainment",
    children: [
      {
        name: "Books",
      },
      {
        name: "Concerts",
      },
      {
        name: "Gambling",
      },
      {
        name: "Gaming",
      },
      {
        name: "Hobbies",
      },
      {
        name: "Film",
      },
      {
        name: "Music",
      },
      {
        name: "Outdoor Activities",
      },
      {
        name: "Photography",
      },
      {
        name: "Sports",
      },
      {
        name: "Theater",
      },
      {
        name: "Streaming",
      },
    ],
  },
  {
    name: "Every Day",
    children: [
      {
        name: "Groceries",
      },
      {
        name: "Restaurants",
      },
      {
        name: "Personal Needs",
      },
      {
        name: "Clothes",
      },
      {
        name: "Dry Cleaning",
      },
      {
        name: "Subscriptions",
      },
    ],
  },
  {
    name: "Gifts",
    children: [
      {
        name: "Gifts",
      },
      {
        name: "Charity",
      },
    ],
  },
  {
    name: "Health",
    children: [
      {
        name: "Doctor",
      },
      {
        name: "Dentist",
      },
      {
        name: "Optician",
      },
      {
        name: "Special Care",
      },
      {
        name: "Pharmacy",
      },
      {
        name: "Emergencies",
      },
    ],
  },
  {
    name: "Home",
    children: [
      {
        name: "Rent/Fee",
      },
      {
        name: "Property Tax",
      },
      {
        name: "Furniture",
      },
      {
        name: "Garden",
      },
      {
        name: "Maintenance",
      },
      {
        name: "Repairs",
      },
      {
        name: "Move",
      },
    ],
  },
  {
    name: "Insurance",
    children: [
      {
        name: "Car",
      },
      {
        name: "Health",
      },
      {
        name: "Home",
      },
      {
        name: "Life",
      },
      {
        name: "Unemployment",
      },
    ],
  },
  {
    name: "Pets",
    children: [
      {
        name: "Food",
      },
      {
        name: "Veterinary",
      },
      {
        name: "Medicine",
      },
      {
        name: "Toys",
      },
    ],
  },
  {
    name: "Tech",
    children: [
      {
        name: "Domains and Hosting",
      },
      {
        name: "Web Services",
      },
      {
        name: "Hardware",
      },
      {
        name: "Software",
      },
    ],
  },
  {
    name: "Transport",
    children: [
      {
        name: "Fuel",
      },
      {
        name: "Car Installments",
      },
      {
        name: "Repairs",
      },
      {
        name: "License",
      },
      {
        name: "Public Transport",
      },
    ],
  },
  {
    name: "Travel",
    children: [
      {
        name: "Airline Tickets",
      },
      {
        name: "Hotel",
      },
      {
        name: "Food",
      },
      {
        name: "Transport",
      },
      {
        name: "Entertainment",
      },
    ],
  },
  {
    name: "Electricity / Water",
    children: [
      {
        name: "Phone",
      },
      {
        name: "Tv",
      },
      {
        name: "Broadband",
      },
      {
        name: "Electricity",
      },
      {
        name: "Heat / Gas",
      },
      {
        name: "Water",
      },
      {
        name: "Garbage Collection",
      },
    ],
  },
  {
    name: "Other",
    children: [
      {
        name: "Other",
      },
    ],
  },
  {
    name: "Salary",
    children: [
      {
        name: "Salary Payment",
      },
      {
        name: "Tips",
      },
      {
        name: "Bonus",
      },
      {
        name: "Commission",
      },
    ],
  },
  {
    name: "Income Sources",
    children: [
      {
        name: 'Interest Income'
      },
      {
        name: 'Dividend'
      },
      {
        name: 'Cash Gift'
      },
      {
        name: 'Refund'
      },
    ]
  }
];
