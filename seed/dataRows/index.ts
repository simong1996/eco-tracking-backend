import { Connection, createConnection } from "typeorm";

import fs from "fs";
import { parse } from "csv-parse";

import { Datarow, TransactionType } from "../../src/entities/Datarow";
import { Category } from "../../src/entities/Category";
import { Account } from "../../src/entities/Account";
import { User } from "../../src/entities/User";

interface Row {
  amount: string;
  date: string;
  identifier: string;
  description: string;
  isUnusual: string;
  type: string;
  categoryId: string;
  userId: string;
  toAccountId: string;
  fromAccountId: string;
}

let typeormCon: Connection;

const getType = async (type: string) => {
  switch (type) {
    case "EXPENSE":
      return "expense";
    case "TRANSFER":
      return "transfer";
    case "INCOME":
      return "income";
    default:
      return "expense";
  }
};

(async () => {
  console.log("Opening db connection...");

  typeormCon = await createConnection({
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "",
    password: "",
    database: "ecomatic",
    entities: ["src/entities/*.ts"],
  });

  const csvData: Row[] = [];
  fs.createReadStream("./seed/dataRows/test.csv")
    .pipe(parse({ delimiter: "," }))
    .on("data", async (csvrow: string[]) => {
      const csvRowObj: Row = {
        amount: csvrow[0],
        date: csvrow[1],
        identifier: csvrow[2],
        description: csvrow[3],
        isUnusual: csvrow[4],
        type: csvrow[5],
        categoryId: csvrow[7],
        userId: csvrow[8],
        toAccountId: csvrow[10],
        fromAccountId: csvrow[12],
      };

      csvData.push(csvRowObj);
    })
    .on("end", async () => {
      const user = await User.findOne({
        where: {
          id: "bf3b9310-f9c8-4511-92d5-92f1d3de4bf9",
        },
      });
      console.log("Adding to user: ", user?.email);

      for (let i = 0; i < csvData.length; i++) {
        const row = csvData[i];

        const category = await Category.findOne({
          where: { id: row.categoryId },
        });

        console.log("Adding row with category: ", category?.name);

        let toAccount;
        if (row.toAccountId) {
          toAccount = await Account.findOne({
            where: { id: row.toAccountId },
          });
        }

        console.log("Adding funds to: ", toAccount?.name);

        let fromAccount;
        if (row.fromAccountId) {
          fromAccount = await Account.findOne({
            where: { id: row.fromAccountId },
          });
        }

        console.log("Withdrawing funds from: ", fromAccount?.name);

        const transactionType: TransactionType = await getType(row.type);
        const test1 = row.amount.replace(",", ".").replace(/\s/g, '');
        const amount = Number(test1);

        const dataRow = await Datarow.create({
          amount: amount,
          category,
          description: row.description,
          identifier: row.identifier,
          isUnusual: row.isUnusual === "TRUE",
          fromAccount,
          toAccount,
          transactionDate: new Date(row.date),
          transactionType: transactionType,
          user,
        }).save();

        console.log(
          "Created datarow: ",
          dataRow.amount,
          " SEK, ",
          dataRow.transactionDate,
          " ",
          dataRow.identifier
        );
      }
      console.log("Seeding finished.");
      console.log("Closing db connection...");
      typeormCon.close();
    });
})();
